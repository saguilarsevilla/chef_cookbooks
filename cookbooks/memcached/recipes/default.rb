#
# Cookbook Name:: memcached
# Recipe:: default
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalar memcached
package "memcached" do
    action :install
end