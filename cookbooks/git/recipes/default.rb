#
# Cookbook Name:: git
# Recipe:: default
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalamos git
package "git-core" do
    action :install
end