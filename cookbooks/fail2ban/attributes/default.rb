#
# Cookbook Name:: fail2ban
# Recipe:: default
# Attributes for fail2ban.conf & jail.conf
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#

# fail2ban.conf - opciones de configuración
default['fail2ban']['loglevel'] = 3
default['fail2ban']['socket'] = '/var/run/fail2ban/fail2ban.sock'
default['fail2ban']['logtarget'] = '/var/log/fail2ban.log'

# These values will only be printed to fail2ban.conf
# if node['fail2ban']['logtarget'] is set to 'SYSLOG'
default['fail2ban']['syslog_target'] = '/var/log/fail2ban.log'
default['fail2ban']['syslog_facility'] = '1'

# jail.conf - opciones de configuración
default['fail2ban']['ignoreip'] = '127.0.0.1/8'
default['fail2ban']['bantime'] = 86400
default['fail2ban']['maxretry'] = 5
default['fail2ban']['backend'] = 'polling'
default['fail2ban']['email'] = 'domains@iliux.com'
default['fail2ban']['action'] = 'action_mwl'
default['fail2ban']['banaction'] = 'iptables-multiport'
default['fail2ban']['mta'] = 'sendmail'
default['fail2ban']['protocol'] = 'tcp'
default['fail2ban']['chain'] = 'INPUT'

case node['platform_family']
when 'rhel'
  default['fail2ban']['auth_log'] = '/var/log/secure'
when 'debian'
  default['fail2ban']['auth_log'] = '/var/log/auth.log'
end

default['fail2ban']['services'] = {
# filtro de SSH
  'ssh' => {
        'enabled' => 'true',
        'port' => 'ssh',
        'filter' => 'sshd',
        'logpath' => node['fail2ban']['auth_log'],
        'maxretry' => '6'
  },
# filtro de ataque DDOS a SSH
#  {
  'ssh-ddos' => {
        'enabled' => 'true',
        'port' => 'ssh',
        'filter' => 'ssh-ddos',
        'logpath' => node['fail2ban']['auth_log'],
        'maxretry' => '6'
  },
# filtro pure-ftp
  'pure-ftp' => {
        'enabled' => 'true',
        'port' => 'ftp',
        #'ftps','ftps-data',
        'filter' => 'pure-ftp',
        'logpath' => node['fail2ban']['auth_log'],
        'maxretry' => '6',
  },
# filtro para mysql
  'mysql' => {
        'enabled' => 'true',
        'port' => '3306',
        'filter' => 'mysql',
        'logpath' => node['fail2ban']['auth_log'],
        'maxretry' => '6'
  },
}
