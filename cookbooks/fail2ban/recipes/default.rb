#
# Cookbook Name:: fail2ban
# Recipe:: fail2ban
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalar Fail2Ban en el servidor destino
package "fail2ban" do
    action :install
end
#
# Plantilla para configuracion de fail2ban
template "/etc/fail2ban/fail2ban.conf" do
    source "fail2ban.conf.erb"
    owner "root"
    group "root"
    mode 0644
    notifies :restart, 'service[fail2ban]'
end
#
# Plantilla para configurar los jails de fail2ban
template "/etc/fail2ban/jail.conf" do
    source "jail.conf.erb"
    owner "root"
    group "root"
    mode 0644
    notifies :restart, 'service[fail2ban]'
end
# Plantilla para configurar el correo saliente de fail2ban
template "/etc/fail2ban/action.d/sendmail-whois-lines.conf" do
    source "sendmail-whois-lines.erb"
    owner "root"
    group "root"
    mode 0644
    notifies :restart, 'service[fail2ban]'
end
# Iniciar servicio de Fail2Ban


# Make sure the service starts on reboot
service "fail2ban" do
    supports [:status => true, :restart => true]
    action [:start, :enable]
end
# test
