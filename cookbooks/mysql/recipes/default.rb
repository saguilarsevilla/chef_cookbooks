#
# Cookbook Name:: MySQL
# Recipe:: default
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalar MySQL
package "mysql-server" do
    action :install
end

# Plantilla para mysql.conf
template '/etc/init/mysql.conf' do
  source 'init-mysql.conf.erb'
end

# Plantilla para my.conf
template '/etc/mysql/my.cnf' do
  source 'my.cnf.erb'
  owner 'root'
  group 'root'
  mode '0644'
  notifies :run, 'bash[move mysql data to datadir]', :immediately
  notifies :reload, 'service[mysql]'
end

#
service 'mysql' do
  service_name 'mysql'
  supports     :status => true, :restart => true, :reload => true
  action       [:enable, :start]
end