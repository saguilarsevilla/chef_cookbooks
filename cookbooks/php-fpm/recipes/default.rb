#
# Cookbook Name:: PHP-FPM
# Recipe:: default
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalar php5-fpm
package "php5-fpm" do
    action :install
end

# Ahora vamos a instalar las dependencias/modulos que usamos para PHP
#
# Instalamos PHP Tidy
package "php5-tidy" do
    action :install
end
#
# Instalamos PHP MCrypt
package "php5-mcrypt" do
    action :install
end
#
# Instalamos PHP MySQL
package "php5-mysql" do
    action :install
end
#
# Instalamos PHP Memcached
package "php5-memcached" do
    action :install
end
#
# Instalamos PHP Curl
package "php5-curl" do
    action :install
end
# Instalamos PHP Soap
package "php5-soap" do
	action :install
end
