#
# Cookbook Name:: NGINX
# Recipe:: default
#
# Copyright 2014, Salvador Aguilar & Iliux SA de CV
#
# All rights reserved - Do Not Redistribute
#
#
# Instalar NGINX
package "nginx" do
    action :install
end