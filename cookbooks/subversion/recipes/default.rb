##
 # Cookbook Name:: Subversion
 # Recipe:: default
 #
 # Copyright 2014, Salvador Aguilar & Iliux SA de CV
 #
 # All rights reserved - Do Not Redistribute
 #
 #
 # Instalamos Subversion
 package "subversion" do
    action :install
 end