# See http://docs.opscode.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "riper81"
client_key               "#{current_dir}/riper81.pem"
validation_client_name   "iliux-validator"
validation_key           "#{current_dir}/iliux-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/iliux"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]
